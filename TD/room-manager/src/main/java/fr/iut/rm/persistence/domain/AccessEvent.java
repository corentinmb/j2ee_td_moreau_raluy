package fr.iut.rm.persistence.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "accessevent")
public class AccessEvent {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String name;


    @Column(nullable = false)
    private EventType type;


    @Column(nullable = false)
    private Date date;

    @ManyToOne
    private Room room;

    public AccessEvent(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}

package fr.iut.rm.persistence.dao.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import fr.iut.rm.persistence.dao.AccessEventDao;
import fr.iut.rm.persistence.domain.AccessEvent;
import fr.iut.rm.persistence.domain.AccessEvent_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class AccessEventDaoImpl implements AccessEventDao{
    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(AccessEventDaoImpl.class);

    /**
     * Entity Manager used to perform database operations
     */
    @Inject
    private Provider<EntityManager> em;

    /**
     * @param ae AccessEvent to save or update
     */
    @Override
    @Transactional
    public void saveOrUpdate(final AccessEvent ae) {
        this.em.get().persist(ae);
        logger.debug("access '{}' saved", ae.getName());
    }

    /**
     * @return the entire db accessEvent list
     */
    @Override
    @Transactional
    public List<AccessEvent> findAll() {
        StringBuilder query = new StringBuilder("from ");
        query.append(AccessEvent.class.getName());
        List<AccessEvent> accessEvents = em.get().createQuery(query.toString()).getResultList();
        logger.debug("{} access found", accessEvents);
        return accessEvents;
    }


    /**
     * @param name  of accessEvent
     * @return the corresponding ae or null if nothing found
     */
    @Override
    @Transactional
    public AccessEvent findByName(final String name) {
        StringBuilder query = new StringBuilder("from ");
        query.append(AccessEvent.class.getName()).append(" as ae");
        query.append(" where ae.").append(AccessEvent_.name.getName()).append(" = :name");

        List<AccessEvent> resultList = em.get().createQuery(query.toString()).setParameter("name", name).getResultList();

        if (resultList.size() > 0) {
            logger.debug("ae  with name '{}' found", name);
            return (AccessEvent) resultList.get(0);
        }
        logger.debug("No ae with name '{}' found", name);
        return null;
    }

}

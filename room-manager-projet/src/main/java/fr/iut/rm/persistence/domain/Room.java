package fr.iut.rm.persistence.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Room's name
     */
    @Column(nullable = false, unique = true)
    @Length(min=3,max=30)
    private String name;

    @Column(nullable = false, unique = false)
    @Min(1)
    private int capacity = 1;

    @Column(nullable = false, unique = false)
    @Min(0)
    private int people = 0;

    /**
     * Default constructor (do nothing)
     */
    public Room() {
        // do nothing
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * anemic getter
     *
     * @return the capacity
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * anemic setter
     *
     * @param capacity the new capacity number to set
     */
    public void setCapacity(final int capacity) {
        this.capacity = capacity;
    }

    /**
     * anemic getter
     *
     * @return the people counter
     */
    public int getPeople() {
        return people;
    }

    /**
     * anemic setter
     *
     * @param people the new people number to set
     */
    public void setPeople(final int people) {
        this.people = people;
    }

    /**
     * anemic in method
     */
    public void peopleIn(){
        if(this.people + 1 <= this.capacity)
            this.people++;
    }

    /**
     * aneminc out method
     */
    public void peopleOut(){
        if(this.people - 1 >= 0)
            this.people--;
    }

    public int getRatio(){
        float result = ((float)people/(float)capacity)*100;
        return (int)result;
    }

}

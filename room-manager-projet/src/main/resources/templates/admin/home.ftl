<#include "assets/header-admin.ftl">

<div class="container">
    <div class="row clearfix">
        <#list rooms as room>
            <div class="modal fade bs-example-modal-sm" id="${room.getId()}modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Confirm deletion</h4>
                        </div>
                        <div class="modal-body">
                            <h5>The room named ${room.getName()} will be deleted and you'll loose all data about this room.<br/><br/>Are you sure you want to delete this room ?</h5>
                        </div>
                        <div class="modal-footer">
                            <a type="button" class="btn btn-default" data-dismiss="modal">Close</a>
                            <a href="delete-room?name=${room.getName()}" type="button" class="btn btn-primary">Confirm</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <#if (room.getRatio() < 40)>
                    <div class="panel panel-success">
                <#elseif (room.getRatio() >= 40 && room.getRatio() < 70)>
                    <div class="panel panel-warning">
                <#elseif (room.getRatio() >= 70)>
                    <div class="panel panel-danger">
                </#if>
                    <div class="panel-heading text-center">
                    <a data-toggle="modal" data-target="#${room.getId()}modal" class="btn btn-danger btn-sm pull-right" style="position:absolute;left:91%;top:-5%;">
                        <span style="vertical-align:-0.5px" class="glyphicon glyphicon-remove"></span>
                    </a>
                    <h2>${room.getName()}</h2>
                    </div>
                    <div class="panel-body text-center">
                        <p>People in this room: ${room.getPeople()}</p>
                        <p>Capacity: ${room.getCapacity()}</p>
                    </div>
                    <div class="panel-footer">
                        <div class="btn-group" role="group" aria-label="myGroup">
                            <div class="btn-group pull-left" role="group">
                                <a href="io?name=${room.getName()}&type=in" type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                            </div>
                            <div class="btn-group pull-left" role="group">
                                <a href="io?name=${room.getName()}&type=out" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                            </div>
                        </div>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                QRCode <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="../qrcode?name=${room.getName()}&type=in">In QRCode</a></li>
                                <li><a href="../qrcode?name=${room.getName()}&type=out">Out QRCode</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </#list>
    </div>
</div> <!-- /container -->

<#include "assets/end-admin.ftl">

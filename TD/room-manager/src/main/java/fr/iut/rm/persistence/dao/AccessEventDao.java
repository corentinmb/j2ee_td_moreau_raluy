package fr.iut.rm.persistence.dao;

import com.google.inject.persist.Transactional;
import fr.iut.rm.persistence.domain.AccessEvent;

import java.util.List;

public interface AccessEventDao {
    void saveOrUpdate(AccessEvent ae);

    List<AccessEvent> findAll();

    AccessEvent findByName(String name);
}

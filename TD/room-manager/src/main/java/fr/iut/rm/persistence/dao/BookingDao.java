package fr.iut.rm.persistence.dao;

import com.google.inject.persist.Transactional;
import fr.iut.rm.persistence.domain.Booking;

import java.util.List;

public interface BookingDao {
    @Transactional
    void saveOrUpdate(Booking booking);

    @Transactional
    List<Booking> findAll();

    @Transactional
    Booking findByName(String name);
}

package fr.iut.Bean;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

public class Document {
    @Past
    private
    Date creationDate;

    @Past
    private
    Date lastModificationDate;

    @NotNull
    private
    Person creator;

    @NotNull
    private
    Person lastModifier;

    @NotNull
    private
    String content;

    @NotNull
    private
    String title;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public Person getCreator() {
        return creator;
    }

    public void setCreator(Person creator) {
        this.creator = creator;
    }

    public Person getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(Person lastModifier) {
        this.lastModifier = lastModifier;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Document(){
        creationDate = new Date();
        lastModificationDate = new Date();
        creator = new Person();
        lastModifier = new Person();
        content = "";
        title = "";
    }
}

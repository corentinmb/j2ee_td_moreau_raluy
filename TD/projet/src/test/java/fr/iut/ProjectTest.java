package fr.iut;

import fr.iut.Bean.Document;
import fr.iut.Bean.Person;
import fr.iut.Bean.Project;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.HashSet;
import java.util.Set;

public class ProjectTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        ProjectTest.validator = factory.getValidator();
    }

    @Test
    public void goodProjectConfiguration(){
        Project p = new Project();
        p.setCustomers(new HashSet<Person>());
        p.setDocuments(new HashSet<Document>());
        p.setStudents(new HashSet<Person>());
        p.setTeachers(new HashSet<Person>());
        p.setName("J2EE");
        p.setDescription("A cool project.");

        Set<ConstraintViolation<Project>> violations = validator.validate(p);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void badProjectConfiguration(){
        Project p = new Project();
        p.setCustomers(null);
        p.setDocuments(null); // can be null.
        p.setStudents(new HashSet<Person>());
        p.setTeachers(null);
        p.setName("J2EE");
        p.setDescription(null);

        Set<ConstraintViolation<Project>> violations = validator.validate(p);
        Assert.assertEquals(3, violations.size());
    }
}

package fr.iut.rm.persistence.domain;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Booking.class)
public abstract class Booking_ {

	public static volatile SingularAttribute<Booking, Date> dateStart;
	public static volatile SingularAttribute<Booking, String> name;
	public static volatile SingularAttribute<Booking, Long> id;
	public static volatile SingularAttribute<Booking, Date> dateEnd;
	public static volatile SingularAttribute<Booking, Room> room;

}


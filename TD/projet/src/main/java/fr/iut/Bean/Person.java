package fr.iut.Bean;

import fr.iut.validation.Login;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

public class Person {

    @Email
    private String email;

    @NotNull
    private String firstname;

    @NotNull
    private String lastname;

    @Login
    private String login;

    private Boolean isStudent;

    public Person(){
        email = "";
        firstname = "";
        lastname = "";
        login = "";
        isStudent = true;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Boolean getIsStudent() {
        return isStudent;
    }

    public void setIsStudent(Boolean isStudent) {
        this.isStudent = isStudent;
    }
}

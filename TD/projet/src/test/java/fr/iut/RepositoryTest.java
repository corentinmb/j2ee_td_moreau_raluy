package fr.iut;

import fr.iut.Bean.Person;
import fr.iut.Bean.Repository;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class RepositoryTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        RepositoryTest.validator = factory.getValidator();
    }

    @Test
    public void goodRepositoryURL(){
        Repository r = new Repository();
        r.setUrl("http://bitbucket.org/loullouise/j2ee_td_moreau_raluy");

        Set<ConstraintViolation<Repository>> violations = validator.validate(r);
        Assert.assertEquals(0, violations.size());
    }

    @Test
    public void badRepositoryURL(){
        Repository r = new Repository();
        r.setUrl("ftp://bitbucket.org/loullouise/j2ee_td_moreau_raluy");

        Set<ConstraintViolation<Repository>> violations = validator.validate(r);
        Assert.assertEquals(1, violations.size());
    }
}

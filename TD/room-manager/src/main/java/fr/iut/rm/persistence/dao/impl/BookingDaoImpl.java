package fr.iut.rm.persistence.dao.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.persist.Transactional;
import fr.iut.rm.persistence.dao.BookingDao;
import fr.iut.rm.persistence.domain.Booking;
import fr.iut.rm.persistence.domain.Booking_;
import fr.iut.rm.persistence.domain.Room_;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class BookingDaoImpl implements BookingDao{

    /**
     * Logger
     */
    private static final Logger logger = LoggerFactory.getLogger(BookingDaoImpl.class);

    /**
     * Entity Manager used to perform database operations
     */
    @Inject
    private Provider<EntityManager> em;

    /**
     * @param booking Booking to save or update
     */
    @Override
    @Transactional
    public void saveOrUpdate(final Booking booking) {
        this.em.get().persist(booking);
        logger.debug("Room '{}' saved", booking.getName());
    }

    /**
     * @return the entire db booking list
     */
    @Override
    @Transactional
    public List<Booking> findAll() {
        StringBuilder query = new StringBuilder("from ");
        query.append(Booking.class.getName());
        List<Booking> bookings = em.get().createQuery(query.toString()).getResultList();
        logger.debug("{} rooms found", bookings);
        return bookings;
    }


    /**
     * @param name  of booking
     * @return the corresponding booking or null if nothing found
     */
    @Override
    @Transactional
    public Booking findByName(final String name) {
        StringBuilder query = new StringBuilder("from ");
        query.append(Booking.class.getName()).append(" as booking");
        query.append(" where booking.").append(Booking_.name.getName()).append(" = :name");

        List<Booking> resultList = em.get().createQuery(query.toString()).setParameter("name", name).getResultList();

        if (resultList.size() > 0) {
            logger.debug("booking  with name '{}' found", name);
            return (Booking) resultList.get(0);
        }
        logger.debug("No booking with name '{}' found", name);
        return null;
    }

}

<#include "assets/header-admin.ftl">

<div class="container">
    <div class="row clearfix col-md-4" style="float:none; margin: 0 auto;">
        <form class="form-horizontal" method="GET">
            <fieldset>
                <legend>Create a room</legend>
                <div class="form-group">
                    <label>Room's name</label>
                    <div class="forms">
                        <input id="name" name="name" type="text" placeholder="30 characters maximum" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label>Room's capacity</label>
                    <div class="forms">
                        <input id="capacity" name="capacity" type="text" placeholder="Greather than 0, of course..." class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label></label>
                    <div class="forms">
                        <button type="submit" class="btn btn-success">Create a room</button>
                        <button type="reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div> <!-- /container -->

<#include "assets/end-admin.ftl">

